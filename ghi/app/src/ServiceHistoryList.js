import { useEffect, useState } from 'react';
// import './index.css'

// Special Feature 1 - Will need to be changed here when reach final solution!
function IsVip({vipStatus}) {
    let vip = vipStatus;
    let itemContent = "yes";
    if (!vip) {
        itemContent = "no";
    }
    return <td className="item">{itemContent}</td>;
}

function ServiceHistoryList() {
  const [appointments, setAppointments] = useState([])
  // Added lines for searching by Vin
  const [searchVin, setSearchVin] = useState('');
  const [filteredAppointments, setFilteredAppointments] = useState([]);

  const fetchAppointments = async () => {
    const response = await fetch('http://localhost:8080/api/appointments/');
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments)
    }
  }

  // Handler for the search input field
  const handleSearchInputChange = (event) => {
    setSearchVin(event.target.value);
  };

  // Function to filter appointments by VIN
  const handleSearch = () => {
    const filtered = appointments.filter((appointment) => appointment.vin.includes(searchVin));
    setFilteredAppointments(filtered);
  };

  // Initially we want to show all appointments, so we fill the filtered
  // appointments after the initial fetch
  useEffect(() => {
    setFilteredAppointments(appointments);
  }, [appointments]);


  // Use fetchAppointments when empty array (i.e. when page loads)
  useEffect(()=>{
    fetchAppointments();
  }, [])


  // Transforms the date as it's displayed on the page into something more visibly pleasing than the raw data
  const formatDate = (dateString) => {
    const options = { year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute: '2-digit'};
    return new Date(dateString).toLocaleString('en-US', options);
  }

  return (
    <div className="appointment-list-container">
      <h1>Service History</h1>
      <div>
        <input type = "text" placeholder="Enter VIN to search" value={searchVin} onChange={handleSearchInputChange} />
        <button onClick={handleSearch}>Search</button>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date and Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
            {filteredAppointments.map(appointment => (
                <tr key={appointment.id}>
                    <td>{ appointment.vin }</td>
                    <IsVip />
                    <td>{ appointment.customer }</td>
                    <td>{ formatDate(appointment.date_time)}</td>
                    <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                    <td>{ appointment.reason }</td>
                    <td>{ appointment.status }</td>
                </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistoryList;