import React, { useEffect, useState } from "react";

function ListAllCustomers() {
  const [customers, setCustomers] = useState([]);

  async function fetchCustomers() {
    const customersUrl = "http://localhost:8090/api/customers/";
    const response = await fetch(customersUrl);

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    } else {
      alert("Error fetching customer data");
    }
  }

  useEffect(() => {
    fetchCustomers();
  }, []);

  return (
    <>
    <h1>Customers</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Address</th>
          </tr>
        </thead>
        <tbody>
          {customers?.map((customer) => {
            return (
              <tr key={customer.id}>
                <td>{customer.first_name}</td>
                <td>{customer.last_name}</td>
                <td>{customer.phone_number}</td>
                <td>{customer.address}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default ListAllCustomers;
