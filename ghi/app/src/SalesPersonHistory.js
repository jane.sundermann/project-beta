import React, { useEffect, useState } from "react";

function SalesPersonHistory() {
  const [sales, setSales] = useState([]);
  const [salespeople, setSalesPeople] = useState([]);
  const [selectedsalesperson, setSelectedSalesPerson] = useState("");
  const [setdisplay, setDisplay] = useState([]);

  async function fetchSales() {
    const salesurl = "http://localhost:8090/api/sales/";
    const response = await fetch(salesurl);

    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
      setDisplay(sales)
    } else {
      alert("Error fetching data");
    }
    const salespeopleurl = "http://localhost:8090/api/salespeople/";
    const salespeopleresponse = await fetch(salespeopleurl);

    if (salespeopleresponse.ok) {
      const data = await salespeopleresponse.json();
      setSalesPeople(data.sales_person);
    } else {
      alert("Error fetching salespeople");
    }
  }

  useEffect(() => {
    fetchSales();
  }, []);

  function handleSalepeopleChange(event) {
    const { value } = event.target;
    setSelectedSalesPerson(value);
  }

  return (
    <>
    <h1>Salesperson History</h1>
      <div className="mb-3">
        <select
          onChange={handleSalepeopleChange}
          required
          name="salespeople"
          id="salespeople"
          className="form-select"
        >
          <option value="">Choose a Salesperson</option>
          {salespeople?.map((sales_people) => {
            return (
              <option key={sales_people.id} value={sales_people.id}>
                {sales_people.first_name} {sales_people.last_name}
              </option>
            );
          })}
        </select>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>Vin</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales?.map((sale) => {
            return (
              <tr key={sale.id}>
                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                <td>
                  {sale.customer.first_name} {sale.customer.last_name}
                </td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default SalesPersonHistory;
