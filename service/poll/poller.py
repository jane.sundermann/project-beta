import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here. Ignore vs-code error hinting
# from service_rest.models import Something
from service_rest.models import AutomobileVO

def get_automobiles():
    response = requests.get("http://project-beta-inventory-api-1:8000/api/automobiles/")
    content = json.loads(response.content)
    print(content)
    for automobile in content["autos"]:
        AutomobileVO.objects.update_or_create(
            vin=automobile["vin"],
            href=automobile["href"],
        )

def poll(repeat=True):
    while True:
        # print('Service poller polling for data')
        try:
            # Write your polling logic, here
            # Do not copy entire file
            # Added line below to test
            get_automobiles()
            print('Service poller polling for data')

        except Exception as e:
            print(e, file=sys.stderr)

        if (not repeat):
            break

        # updates every 60 seconds
        time.sleep(60)


if __name__ == "__main__":
    poll()


# # Instructions from Learn:
# "You must complete the code for an automobile poller. It should update your AutomobileVO every 60 seconds
# with updated VINs from your Inventory service. Only update code where indicated by comments within
# sales/poll/poller.py. Do not change the order of the code. Feel free to ignore the VS Code error highlights
# on any import statement. Your poller should be able to pass the included tests. You can find your API tests
# within the service/poller/test_poller.py file. You can run your poller tests from within your Docker Service
# poller container using python test_poller.py.”
