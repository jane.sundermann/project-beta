# Created this file blank, "urls.py", for the Service microservice

from django.urls import path
from .views import technician, appointments, canceled, finished

# Django uses the urlpatterns list, created below, to match browser URLs to the
# appropriate view functions that should handle the requests
urlpatterns = [
    # Maps any requests to the URL path 'technicians/' TO the 'technician' view function.
    # The 'name' parameter assigns a name to this URL pattern, "technician"
    path("technicians/", technician, name="technician"),
    path("technicians/<int:technician_id>/", technician, name="technician_delete"),
    path("appointments/", appointments, name="appointment"),
    path("appointments/<int:appointment_id>/", appointments, name="appointment_delete"),
    path("appointments/<int:appointment_id>/cancel/", canceled, name="cancel"),
    path("appointments/<int:appointment_id>/finish/", finished, name="finish"),
]